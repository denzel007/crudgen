{% extends "layouts/main.volt" %} 

{% block content %} 

{{ form("$crudPart$/$plural$/create", "method":"post", "autocomplete" : "off", "class" : "form-horizontal") }}
{% for columnName in columnNames %}
    {% if columnName != 'id' %}
    <div class="form-group">

        <label for="field{{columnName}}" class="col-sm-2 control-label">{{columnName}}</label>
        <div class="col-sm-10">
            {{ text_field(columnName, "size" : 30, "class" : "form-control", "id" : columnName) }}
        </div>
    </div> 
  {% endif %}      
{% endfor %}

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        {{ submit_button('Save', 'class': 'btn btn-primary') }}
    </div>
</div>

</form>

{% endblock %}      

    
 {% block js %} 
     {{ partial("_partials/js") }}
    
  <script type="text/javascript">
 $(document).ready(function() {
               
            });
</script>    
 {% endblock %}    
