{% extends "layouts/main.volt" %} 

{% block content %}   
    
<table id="{{tableName}}"  class="table table-responsive">

    <thead>
        {% for columnName in columnNames %}
        <th>{{columnName}}</th>
       {% endfor %}
        <th>Operations</th>
    </thead>

    <tbody>
    </tbody>

</table>
 {% endblock %}      

    
 {% block js %} 
     {{ partial("_partials/js") }}
    
  <script type="text/javascript">
 $(document).ready(function() {
                $('#{{tableName}}').DataTable({
  "sScrollY": "200px",
        "bScrollCollapse": true,
                    serverSide: true,
                    ajax: {
                        url: '/$crudPart$/{{tableName}}/dataTable',
                        method: 'POST'
                    },
                    columns: [
                        {% for columnName in columnNames %}
                                {data: "{{columnName}}"},
                        {% endfor %}
                        {data: "operations"}
                    ]
                });
                $('#{{tableName}}').css("width","100%")
            });
</script>    
 {% endblock %}    