{% extends "layouts/main.volt" %} 

{% block content %} 

{{ form("$crudPart$/$plural$/save", "method":"post", "autocomplete" : "off", "class" : "form-horizontal") }}

{% for columnName in columnNames %}
  {% if columnName != 'id' %}   
    <div class="form-group">
        <label for="field{{columnName}}" class="col-sm-2 control-label">{{columnName}}</label>
        <div class="col-sm-10">
            {{ text_field(columnName, "size" : 30, "class" : "form-control", "id" : columnName) }}
        </div>
    </div> 
  {% else %}
    <div class="form-group">
      <label for="field{{columnName}}" class="col-sm-2 control-label">{{columnName}}</label>
      <div class="col-sm-10">
          {{ text_field(columnName, "size" : 30, "class" : "form-control", "id" : columnName,  'readonly': 'true') }}
      </div>
    </div>     
  {% endif %}      
{% endfor %}

{{ hidden_field("id") }}

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        {{ submit_button('Send', 'class': 'btn btn-primary') }}
    </div>
</div>

</form>

{% endblock %}      

    
 {% block js %} 
     {{ partial("_partials/js") }}
    
  <script type="text/javascript">
 $(document).ready(function() {
               
            });
</script>    
 {% endblock %}  
