<?php
$namespace$
        
use \DataTables\DataTable;
use Phalcon\Mvc\View;
$useFullyQualifiedModelName$

class $className$Controller extends ControllerBase
{
    
    public function initialize()
    {
        parent::initialize();
       // $this->view->setTemplateBefore('main');
      // $this->view->setTemplateBefore('crudgen/main');
       $this->view->setViewsDir($this->config->modules->crudgen->viewsDir);
       $this->view->setTemplateBefore('main');
    }
    
    /**
     * Index action
     */
    public function indexAction()
    {
         $this->persistent->parameters = null;
         $this->view->title = "Dashboard - $className$";
         $this->view->logo = 'CRUD Gen';
         $this->view->header = "Dashboard - $className$";
         $this->view->activeMenuLink = "/$crudPart$/$plural$";
         $this->view->activeMenuClass= "$plural$";
         $this->view->tableName = "$plural$";
         $this->view->buttonLink = "/$crudPart$/$plural$/new";
         $this->view->buttonText = 'Create New';
         $this->view->buttonClass = 'btn-primary';

        $attributes = $this->getAttributes(new $className$); 
        $this->view->columnNames = $attributes;
        //$this->view->pick("crudgen/$plural$/index");
    }
    
    public function dataTableAction()
    {
          $array  = $this->modelsManager->createQuery("SELECT * FROM $fullyQualifiedModelName$")
                             ->execute()->toArray();
          
             foreach ($array as $key => &$value) {
                $this->view->id = $value['id'];
                $this->view->module = "$plural$";
                $this->view->delLink = "$crudPart$/$plural$/delete/".$value['id'];
                $array[$key]['operations'] = $this->view->getPartial('_partials/operations');
             }

          $dataTables = new DataTable();
          $dataTables->fromArray($array)->sendResponse();exit;
    }
    
    /**
     * Deletes a $singular$
     *
     * @param string $pkVar$
     */
    public function deleteAction($pkVar$)
    {
        $singularVar$ = $className$::findFirstBy$pk$($pkVar$);
        if (!$singularVar$) {
            $this->flash->error("$singular$ was not found");

            $this->dispatcher->forward([
                'controller' => "$plural$",
                'action' => 'index'
            ]);

            return;
        }

        if (!$singularVar$->delete()) {

            foreach ($singularVar$->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "$plural$",
                'action' => 'index'
            ]);

            return;
        }

        $this->flash->success("$singular$ was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "$plural$",
            'action' => "index"
        ]);
    }
    
    /**
     * Displays the creation form
     */
    public function newAction()
    {
         $this->view->title = "Dashboard - Create $className$";
         $this->view->logo = "CRUD Gen";
         $this->view->header = "Dashboard - Create $className$";
         $this->view->activeMenuLink = "/$crudPart$/$plural$/new";
         $this->view->activeMenuClass= "$plural$New";
         $this->view->buttonLink = "/$crudPart$/$plural$";
         $this->view->buttonText = "Back";
         $this->view->buttonClass = 'btn-default';
         
         $attributes = $this->getAttributes(new $className$());            

         $this->view->columnNames = $attributes;
         //$this->view->pick("crudgen/$plural$/new");
    }
    
    /**
     * Creates a $singular$
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "$plural$",
                'action' => 'index'
            ]);

            return;
        }

        $singularVar$ = new $className$();
        $attributes = $this->getAttributes(new $className$());            
        foreach ($attributes as $key => $attribute) {
            $singularVar$->$attribute = $this->request->getPost("$attribute");
        }
   
        if (!$singularVar$->save()) {
            foreach ($singularVar$->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "$plural$",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("$singular$ was created successfully");

        $this->dispatcher->forward([
            'controller' => "$plural$",
            'action' => 'index'
        ]);
    }
    
     /**
     * Edits a $plural$
     *
     * @param string $pkVar$
     */
    public function editAction($pkVar$)
    {
        if (!$this->request->isPost()) {

            $singularVar$ = $className$::findFirstByid($pkVar$);
            if (!$singularVar$) {
                $this->flash->error("$singular$ was not found");

                $this->dispatcher->forward([
                    'controller' => "$plural$",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $singularVar$->id;
            $this->view->title = "Dashboard - Edit $className$";
            $this->view->logo = 'CRUD Gen';
            $this->view->header = "Dashboard - Edit $className$";
            $this->view->activeMenuLink = "/$crudPart$/$plural$/edit";
            $this->view->activeMenuClass = "$plural$Edit";
            $this->view->buttonLink = "/$crudPart$/$plural$";
            $this->view->buttonText = 'Back';
            $this->view->buttonClass = 'btn-default';

            $attributes = $this->getAttributes(new $className$());            

            foreach ($attributes as $key => $attribute) {
                $this->tag->setDefault("$attribute", $singularVar$->$attribute);
            }
            $this->view->columnNames = $attributes;
            //$this->view->pick("crudgen/$plural$/edit");
  
        }
    }
    
    /**
     * Saves a $singular$ edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "$plural$",
                'action' => 'index'
            ]);

            return;
        }

        $pkVar$ = $this->request->getPost("$pk$");
        $singularVar$ = $className$::findFirstByid($pkVar$);

        if (!$singularVar$) {
            $this->flash->error("$singular$ does not exist " . $pkVar$);

            $this->dispatcher->forward([
                'controller' => "$plural$",
                'action' => 'index'
            ]);

            return;
        }
        
        $attributes = $this->getAttributes(new $className$());            

        foreach ($attributes as $key => $attribute) {
           $singularVar$->$attribute = $this->request->getPost($attribute);
        }
        
        if (!$singularVar$->save()) {

            foreach ($singularVar$->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "$plural$",
                'action' => 'edit',
                'params' => [$singularVar$->id]
            ]);

            return;
        }

        $this->flash->success("$singular$ was updated successfully");

        $this->dispatcher->forward([
            'controller' => "$plural$",
            'action' => 'index'
        ]);
    }
    
}    
