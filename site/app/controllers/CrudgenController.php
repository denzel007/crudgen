<?php

namespace App\Controllers;

use \Crudgen\CrudgenController as BaseCrudgen;
use  App\Controllers\ControllerBase;

class CrudgenController extends BaseCrudgen
{

    public function initialize()
    {
      # TO DO - check access to this superadmin module
        
      parent::initialize();
      
      $this->view->setViewsDir($this->config->modules->crudgen->viewsDir);
    }
    
    function onConstruct()
    {
        $base = new ControllerBase();
        
        $base->onConstruct();
        
    }    
    
    public function indexAction()
    {
      parent::indexAction();
    }
    
    public function allModulesAction()
    { 
      parent::allModulesAction(); 
    }
    
    public function genAction($moduleName = '') 
    {
      parent::genAction($moduleName);  
    }
    
}

