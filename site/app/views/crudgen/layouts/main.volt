{% extends "layouts/base.volt" %}

{% block title %} {{title}} {% endblock %}

{% block css %} 
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.4/css/jquery.dataTables.min.css">-->

    {{ stylesheet_link('modules/crudgen/css/font-awesome.min.css') }}
    {{ stylesheet_link('modules/crudgen/css/ionicons.min.css') }}
    {{ stylesheet_link('modules/crudgen/css/morris/morris.css') }}
    {{ stylesheet_link('modules/crudgen/css/jvectormap/jquery-jvectormap-1.2.2.css') }}
    {{ stylesheet_link('modules/crudgen/css/datatables/dataTables.bootstrap.css') }}
    {{ stylesheet_link('modules/crudgen/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}
    {{ stylesheet_link('modules/crudgen/css/AdminLTE.css') }}
{% endblock %}

{% block content %}
<header class="header">
    <a href="" class="logo">{{logo}}</a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">

        <!-- Sidebar toggle button-->
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

    </nav>
</header>            

<div class="wrapper row-offcanvas row-offcanvas-left">
    
    <aside class="left-side sidebar-offcanvas">                
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left info">

                </div>
            </div>

            <!-- sidebar menu: : style can be found in sidebar.less -->
 
            {{partial("_partials/sidebarMenu", ['menu': menu])}}
        </section>
        <!-- /.sidebar -->
    </aside> 
    
    <aside class="right-side">                
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    {{header}}
                </h1>
                  {{flash.output()}}
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">

                                                                            
                        <div class="box">
                            <div class="box-header">
                                
                            </div><!-- /.box-header -->

                            <div class="box-body">
                                <a class="btn {{buttonClass}}" href="{{buttonLink}}">{{buttonText}}</a><br /><br />
                                    {{ content() }}
                                    
                            </div>
                        </div> 
                    </div>   
                </div>   
            </section>
     </aside>        

</div>
            
{% endblock %}
   
