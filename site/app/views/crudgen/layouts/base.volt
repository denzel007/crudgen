<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <title>{% block title %} {% endblock %} - CRUD Generator</title>
        {{ stylesheet_link('modules/crudgen/css/bootstrap.min.css') }}
        {{ stylesheet_link('modules/crudgen/css/style.css') }}
        {% block css %} {% endblock %}
</head>

    <body class="skin-black">

                    {% block content %} 
                    {% endblock %}

    {% block js %} 
    {% endblock %}

    {{ partial("_partials/footer") }}   

    </body>
    
</html>