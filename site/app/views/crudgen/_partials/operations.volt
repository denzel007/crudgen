<a href="/{{crudPart}}/{{module}}/edit/{{id}}" class="btn btn-info" >
    <i class="fa fa-edit"></i>
</a>
<br>
<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#del{{module}}{{id}}">
    <i class="fa fa-trash-o"></i>
</a>
<div id="del{{module}}{{id}}" class="modal fade"> 
<div class="modal-dialog">
        <div class="modal-content"> 

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Delete {{module}} with "{{id}}"</h4>
</div>
<div class="modal-body">
    <p>Are you shure?</p>
</div>
    <div class="modal-footer">

         {{ form(delLink, 'method': 'post') }}
 
            {{ submit_button('Yes', 'class':'btn btn-primary') }}
            {{ submit_button('No', 'class':'btn btn-default', 'data-dismiss': 'modal' ) }}
            
        {{ end_form() }}

    </div>
        </div>    
</div>  
</div>  