<ul class="sidebar-menu">

<li {% if activeMenu == 'index' %} class="active" {% endif %}>
    <a href="/crudgen">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
</li>

{% for menuItem in menu %}
    
    <li class="treeview {% if activeMenu == menuItem %} active {% endif %}">
    <a href="#">
        <i class="fa fa-folder-o"></i>
        <span>{{menuItem}}</span>
        <i class="fa pull-right fa-angle-right"></i>
    </a>
    <ul class="treeview-menu" style="display: none;">
        <li ><a href="/{{crudPart}}/{{menuItem}}" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> List</a></li>
        <li ><a href="/{{crudPart}}/{{menuItem}}/new" style="margin-left: 10px;"><i class="fa fa-angle-double-right"></i> Create</a></li>
    </ul>
</li>
    
{% endfor %}    

</ul> 
