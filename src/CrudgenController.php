<?php

namespace Crudgen;

use \DataTables\DataTable;
//use Apps\Crudgen\Models\Modules;
use Crudgen\Phalcongen\Builder\Scaffold as ScaffoldBuilder;

class CrudgenController extends ControllerBase
{

    public function initialize()
    {
         parent::initialize();
         $this->view->setTemplateBefore('main');
         
    }
    
    
    public function indexAction()
    {
         $this->view->title = 'Dashboard';
         $this->view->logo = 'CRUD Gen';
         $this->view->header = 'All Tables List (Modules)';
         $this->view->buttonLink = 'crudgen/genAll';
         $this->view->buttonText = 'GENERATE ALL CRUDES';
         $this->view->buttonClass = 'btn-primary';
         
    }
    
    public function allModulesAction()
    { 
        $tablesQuery = $this->db->query("SHOW TABLES");
         $i = 1;
         while ($table = $tablesQuery->fetch()) {
            $tables[$i]['id'] = $i;
            $tables[$i]['name'] = $table['Tables_in_'.$this->config->database->dbname.''];
            $this->view->moduleName = $table['Tables_in_'.$this->config->database->dbname.''];

            $check = $this->checkIfCrudExist($table['Tables_in_'.$this->config->database->dbname.'']);
            if (!$check) {
              $genOperationHtml = $this->view->getPartial('_partials/generate');
            } else {
              $genOperationHtml = $this->view->getPartial('_partials/regenerate');  
            }
            $tables[$i]['operation'] = $genOperationHtml;
            $i++;
         }

          $dataTables = new DataTable();
          $dataTables->fromArray($tables)->sendResponse();
    }
    
    public function genAction($moduleName = '') 
    {
        $name = $moduleName;
        defined('TEMPLATE_PATH') || define('TEMPLATE_PATH', BASE_PATH . DS .$this->config->modules->crudgen->templatesPath);
        $templatePath = TEMPLATE_PATH;
        $schema = $this->config->database->dbname;
        $templateEngine = $this->config->templateEngine;
        
        $nsModels = $this->config->modules->crudgen->modelsNamespace;
        $nsControllers = $this->config->modules->crudgen->controllersNamespace;
        
        if (!file_exists($this->config->modules->crudgen->controllersDir)) {
            mkdir($this->config->modules->crudgen->controllersDir, 0777, true);
        }
        if (!file_exists($this->config->modules->crudgen->modelsDir)) {
            mkdir($this->config->modules->crudgen->modelsDir, 0777, true);
        }
        $data = [
            'name'                 => $name,
            'schema'               => $schema,
            'templatePath'         => $templatePath,
            'templateEngine'       => $templateEngine,
            'modelsNamespace'      => $nsModels,
            'controllersNamespace' => $nsControllers,
            'modelsDir' => $this->config->modules->crudgen->modelsDir,
        ];
        
        
        
        $scaffoldBuilder = new ScaffoldBuilder($data);
        $r = $scaffoldBuilder->build();
        if ($r) {
           $this->flash->success("CRUD '$moduleName' was credated successfully");
            return $this->response->redirect('crudgen');
        }
    }
    
    public function genAllAction() 
    {
        $tablesQuery = $this->db->query("SHOW TABLES");
         $i = 1;
         while ($table = $tablesQuery->fetch()) {
             $tableName = $table['Tables_in_'.$this->config->database->dbname.''];
             $this->genAction($tableName);
         }
         
       return $this->response->redirect('crudgen');  
    }
    

}

