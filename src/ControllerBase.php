<?php

namespace Crudgen;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
   function initialize()
   {      
      # check access to CRUD Generator by role ID 
      $roleId = $this->session->get('roleId');
      
      $this->view->menu = $this->menuGen();
      $this->view->activeMenu = $this->dispatcher->getControllerName();
      $this->view->crudPart = $this->config->modules->crudgen->crudPart;
      
   } 
   
   public function getAttributes($modelInstanse) 
   {           
      $metadata = $modelInstanse->getModelsMetaData();

      return $metadata->getAttributes($modelInstanse);
   }
   
   protected function checkIfCrudExist($crudName, $all = false)
   {
       if (!file_exists($this->config->modules->crudgen->viewsDirGen)) {
            mkdir($this->config->modules->crudgen->viewsDirGen, 0777, true);
        }
       $files = scandir($this->config->modules->crudgen->viewsDirGen);
        
        if (($files) AND in_array($crudName, $files)) {
             
             return true;
        }
        return false;
    }
    
    protected function menuGen() 
    {
      $ret = [];
      
       $tablesQuery = $this->db->query("SHOW TABLES");
         $i = 0;
         while ($table = $tablesQuery->fetch()) {
             if ($this->checkIfCrudExist($table['Tables_in_'.$this->config->database->dbname.''])) {
               $ret[$i] =  $table['Tables_in_'.$this->config->database->dbname.'']; 
               $i++;
             }
             
         }
      
      return $ret; 
    }
}
